from scipy.stats import norm
import numpy as np
print(norm.pdf(0))

print(norm.pdf(0, loc=5, scale=10))

r = np.random.randn(10)
print(norm.pdf(r))

#since working with logs and additions are quicker than working with multiplication,
print(norm.logpdf(r))

#Integral of pdf
print(norm.cdf(r))